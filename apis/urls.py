from django.urls import path,include
from . import views
from rest_framework import routers

#   router = routers.DefaultRouter()
#   router.register('like', views.LikeView)

urlpatterns = [
    path('like/', views.likeOverview, name="likeOverview"),
    path('like/<int:pk>/', views.likeOverviewDetail, name="like-detail")
]