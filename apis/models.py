from django.db import models

# Create your models here.
class Like(models.Model):
    bookID = models.CharField(max_length=15)
    bookName = models.CharField(max_length=255, null=True)
    like = models.IntegerField()
    thumbnailBig = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.bookName
