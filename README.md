# STORY LANJUTAN REPO
Hi, ini adalah repo story lanjutan untuk matkul PPW. Nanti semua project dari story 7 sampai seterusnya akan di kerjakan di sini.

## Story 7 "Accordion"
Di Story 7 ini kita belajar tentang jquery dan javascript. Tutorialnya banyak di youtube dan di w3school. Menurut saya jangan di biasakan untuk menulis jquery karena kalau lebih mengerti jquery dari pada javascript, bisa lebih sulit untuk mamu membuat fitur yang dimodifikasi.
Jquery itu hanyalah framework yang membantu menulis javascript dengan cepat, seperti halnya bootstrap untuk css. Tetapi untuk beberapa fitur yang mudah,  jquery sangatlah berguna.

## Story 8 "Book Ajax"
Di Story 8 ini kita diminta untuk mengerti tentang Ajax dan WebService, dimana kita diberikan sebuah public APIs dari google book, yang memberikan kembalian sebuah data buku berdasarakan keyword yang di pencarian. Disini kita harus membuat sebuah search box yang di dalamnya user akan mengketikan keyword yang mau di cari, nanti dari situ kita bisa mengambil data bukunya di API yang sudah di sediakan. 

## Story 9 "Register Log in"
Di story 9 ini kita diajarkan mengenai authentikasi kemudian cookies dan javascript dan tetntunya implementasi dari login logout dan register itu sendiri.
