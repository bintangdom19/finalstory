from django.db import models

# Create your models here.
class Comment(models.Model):
    nama = models.CharField(max_length=255)
    komentar = models.TextField()
    created_at = models.DateField(auto_now_add=True)
    color = models.CharField(max_length=7, default="#744f4f")
