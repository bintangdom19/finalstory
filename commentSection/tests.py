from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth import get_user_model
from . import apps
from .models import Comment
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import *
import time

# Create your tests here.
class TDDStory7UnitTest(TestCase):
    # Test App
    def test_apps(self):
        self.assertEqual(apps.CommentsectionConfig.name, 'commentSection')
    
    def setUp(self):
        self.client = Client()
        User = get_user_model()
        self.user = User.objects.create_user('testDebuging', 'testDebuging@testDebuging.com', 'testDebugingPassword')
    
    def testLogin(self):
        self.client.login(username='testDebuging', password='testDebugingPassword')

    # Url is Exits, Comment Section
    def test_comment_section_url_is_exist(self):
        response = Client().get('/commentsection/')
        self.assertEqual(response.status_code, 200)

    # Url is Use Correct Template, Comment Section
    def test_comment_section_url_is_using_corect_template(self):
        response = Client().get('/commentsection/')
        self.assertTemplateUsed(response, "commentSection/index.html")

    # Url is Exits, Create
    def test_create_url_is_exist(self):
        response = Client().get('/commentsection/create')
        self.assertEqual(response.status_code, 200)

   # Url is Use Correct Template, Comment Section
    def test_create_url_is_using_corect_template(self):
        response = Client().get('/commentsection/create')
        self.assertTemplateUsed(response, "commentSection/create.html")

    # Test Store data input
    def test_url_post_can_store_data(self):
        nama = "a"
        komentar = "oke"
        color = ""
        confirm = "true"
        response = Client().post('/commentsection/create', {'nama' : nama, 'komentar' : komentar, 'color' : color,'confirm' : confirm})
        self.assertEqual(response.status_code, 302)

    # Test Error message
    def test_url_can_back_to_home_give_error_message(self):
        confirm = "false"
        response = Client().post('/commentsection/create', {'confirm' : confirm})
        self.assertEqual(response.status_code, 302)

    # Test input data
    def test_model_can_create_new_object(self):
        new_obj = Comment.objects.create(nama='a', komentar='oke', color='')
        test_all_obj = Comment.objects.all().count()
        self.assertEqual(test_all_obj, 1)

    def test_url_can_delete_comment(self):
        new_obj = Comment.objects.create(nama='a', komentar='oke', color='')
        count1 = Comment.objects.all().count()
        response = Client().post('/commentsection/', {'deleteId' : new_obj.id})
        count2 = Comment.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(count1, count2)
    
    def test_url_error_delete_comment(self):
        new_obj = Comment.objects.create(nama='a', komentar='oke', color='')
        count1 = Comment.objects.all().count()
        response = Client().post('/commentsection/', {'deleteId' : 2})
        count2 = Comment.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(count1, count2)
    
    def test_url_can_change_color(self):
        new_obj = Comment.objects.create(nama='a', komentar='oke', color='')
        old_color = new_obj.color
        response = Client().post('/commentsection/', {'color' : '#e6e65e', 'id' : new_obj.id})
        new_obj = Comment.objects.get(id=new_obj.id)
        new_color = new_obj.color
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(old_color, new_color)


class TDDStrory7FunctionalTest(StaticLiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        # kalau mau di push ke gitlab
        # chrome_options.add_argument('--dns-prefetch-disable')
        # chrome_options.add_argument('--no-sandbox')
        # chrome_options.add_argument('--headless')
        # chrome_options.add_argument('disable-gpu')

        # di lokal pake exe, soalnya windows. kalau di gitlab exenya di hapus karena gitlab pake linux
        self.browser  = webdriver.Chrome('chromedriver.exe', chrome_options=chrome_options)
        super(TDDStrory7FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(TDDStrory7FunctionalTest, self).tearDown()

    def test_todo(self):
        browser = self.browser
        browser.get(self.live_server_url +'/commentsection/')
        time.sleep(5)
        self.assertIn("Comment Section", browser.title)
        # test input to form
        name_box = browser.find_element_by_id('name')
        name_box.send_keys("Bintang Dominique")
        comment_box = browser.find_element_by_id('comment')
        comment_box.send_keys("PPW is Fun")
        time.sleep(2)
        comment_box.submit()
        self.assertIn("Confirm", browser.title)
        time.sleep(5)

        # Yes store data
        ok_button = browser.find_element_by_id("yes_btn")
        ok_button.submit()
        time.sleep(5)
        self.assertIn("Comment Section", browser.title)

        # Is the data that we store is in the first place
        last_input_name = browser.find_element_by_css_selector(".comment-list .comment:nth-child(1) .card .blockquote .blockquote-footer").text
        last_input_komentar = browser.find_element_by_css_selector(".comment-list .comment:nth-child(1) .card .blockquote p").text
        self.assertEqual("Bintang Dominique", last_input_name)
        self.assertEqual("PPW is Fun", last_input_komentar)
        time.sleep(5)

        # test input again
        name_box = browser.find_element_by_id('name')
        name_box.send_keys("Simantap2")
        comment_box = browser.find_element_by_id('comment')
        comment_box.send_keys("PPW is not Fun")
        time.sleep(2)
        comment_box.submit()
        self.assertIn("Confirm", browser.title)
        time.sleep(5)

        # Not store data
        ok_button = browser.find_element_by_id("not_btn")
        ok_button.submit()
        self.assertIn("Comment Section", browser.title)
        last_input_name = browser.find_element_by_css_selector(".comment-list .comment:nth-child(1) .card .blockquote .blockquote-footer").text
        last_input_komentar = browser.find_element_by_css_selector(".comment-list .comment:nth-child(1) .card .blockquote p").text
        self.assertNotEqual("Simantap2", last_input_name)
        self.assertNotEqual("PPW is not Fun", last_input_komentar)
        time.sleep(5)

        # Delete Item Succses
        browser.execute_script("window.scrollTo(0, 500)") 
        choiseDelete = browser.find_element_by_css_selector(".comment-list .comment:nth-child(1) .card .blockquote span")
        choiseDelete.click()
        time.sleep(3)
        deleteForm = browser.find_element_by_css_selector("#deleteForm button")
        deleteForm.click()
        time.sleep(3)
        try:
            haveDelete = browser.find_element_by_css_selector(".comment-list .comment card").size() == 0
        except NoSuchElementException:
            haveDelete = True
        self.assertEqual(True, haveDelete)
        time.sleep(5)
