from django.apps import AppConfig


class AccordioConfig(AppConfig):
    name = 'accordio'
