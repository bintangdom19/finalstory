from django.test import TestCase, Client, LiveServerTestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from . import apps
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import *
import time

# Create your tests here.
# UnitTesting
class AccordioUnitTest(TestCase):
    # Test App
    def test_apps(self):
        self.assertEqual(apps.AccordioConfig.name, 'accordio')

    # Test Url is Exist
    def test_accordio_url_is_exist(self):
        response = Client().get('/accordio/')
        self.assertEqual(response.status_code, 200)


# Functional Test
class AccordioFunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()

        # kalau mau di push ke gitlab
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        # kalau di lokal jangan lupa pake exe
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(AccordioFunctionalTest, self).setUp()
    
    def tearDown(self):
        self.browser.quit()
        super(AccordioFunctionalTest, self).tearDown()
    
    def test_todo(self):
        browser = self.browser
        browser.get(self.live_server_url + "/accordio")
        time.sleep(5)
        self.assertIn("Accordio", browser.title)
        browser.execute_script("window.scrollTo(0, 250)")
        time.sleep(5)

        # Test Open Other Accordio
        accordioTitle = browser.find_element_by_css_selector(".accordio:nth-child(2) div.accordio-title .title")
        firstAccordioBody = browser.find_element_by_css_selector(".accordio:nth-child(1) .accordio-body")
        secondAccordioBody = browser.find_element_by_css_selector(".accordio:nth-child(2) .accordio-body")
        
        accordioTitle.click() # Test Click Second Accordio
        secondBodyClass = secondAccordioBody.get_attribute("class") # Ambil semua classnya di accordio body 2
        firstBodyClass = firstAccordioBody.get_attribute("class") # Ambil semua classnya di accordio body 1

        time.sleep(5)
        
        # cek apakah body kedua setelah di click class nya show
        for i in secondBodyClass.split(" "):
            if i == "show":
                secondShow = True
                break
            else:
                secondShow = False

        for x in firstBodyClass.split(" "):
            if x == "show":
                firstShow = True
                break
            else:
                firstShow = False
        
        self.assertEqual(firstShow, False)
        self.assertEqual(secondShow, True)

        # cek apakah fitur naik turun sudah benar
        # Accordio 1
        firstAccordioTitle = browser.find_element_by_css_selector(".accordio:nth-child(1) div.accordio-title .title h1").text
        firstAccordioSubTitle = browser.find_element_by_css_selector(".accordio:nth-child(1) div.accordio-title .title span").text
        firstAccordioBody = browser.find_element_by_css_selector(".accordio:nth-child(1) .accordio-body p").text
        # Accordio 2
        secondAccordioTitle = browser.find_element_by_css_selector(".accordio:nth-child(2) div.accordio-title .title h1").text
        secondAccordioSubTitle = browser.find_element_by_css_selector(".accordio:nth-child(2) div.accordio-title .title span").text
        secondAccordioBody = browser.find_element_by_css_selector(".accordio:nth-child(2) .accordio-body p").text
        
        # Button Accordio 1 Up Click
        browser.find_element_by_css_selector(".accordio:nth-child(2) div.accordio-title .button .button-up").click()
        time.sleep(5)

        # Accordio 1 after click
        afterFirstAccordioTitle = browser.find_element_by_css_selector(".accordio:nth-child(1) div.accordio-title .title h1").text
        afterFirstAccordioSubTitle = browser.find_element_by_css_selector(".accordio:nth-child(1) div.accordio-title .title span").text

        # Accordio 2
        afterSecondAccordioTitle = browser.find_element_by_css_selector(".accordio:nth-child(2) div.accordio-title .title h1").text
        afterSecondAccordioSubTitle = browser.find_element_by_css_selector(".accordio:nth-child(2) div.accordio-title .title span").text

        # Check apakah sudah berubah atau belum
        self.assertNotEqual(firstAccordioTitle, afterFirstAccordioTitle)
        self.assertNotEqual(firstAccordioSubTitle, afterFirstAccordioSubTitle)

        self.assertNotEqual(secondAccordioTitle, afterSecondAccordioTitle)
        self.assertNotEqual(secondAccordioSubTitle, afterSecondAccordioSubTitle)

        # Yang kedua untuk turun sama aja jadi copy aja, beda buttonnya doang
        # Accordio 1
        firstAccordioTitle = browser.find_element_by_css_selector(".accordio:nth-child(1) div.accordio-title .title h1").text
        firstAccordioSubTitle = browser.find_element_by_css_selector(".accordio:nth-child(1) div.accordio-title .title span").text
        # Accordio 2
        secondAccordioTitle = browser.find_element_by_css_selector(".accordio:nth-child(2) div.accordio-title .title h1").text
        secondAccordioSubTitle = browser.find_element_by_css_selector(".accordio:nth-child(2) div.accordio-title .title span").text
        
        # Button Accordio 1 Up Click
        browser.find_element_by_css_selector(".accordio:nth-child(1) div.accordio-title .button .button-down").click()
        time.sleep(5)

        # Accordio 1 after click
        afterFirstAccordioTitle = browser.find_element_by_css_selector(".accordio:nth-child(1) div.accordio-title .title h1").text
        afterFirstAccordioSubTitle = browser.find_element_by_css_selector(".accordio:nth-child(1) div.accordio-title .title span").text
        # Accordio 2
        afterSecondAccordioTitle = browser.find_element_by_css_selector(".accordio:nth-child(2) div.accordio-title .title h1").text
        afterSecondAccordioSubTitle = browser.find_element_by_css_selector(".accordio:nth-child(2) div.accordio-title .title span").text

        # Check apakah sudah berubah atau belum
        self.assertNotEqual(firstAccordioTitle, afterFirstAccordioTitle)
        self.assertNotEqual(firstAccordioSubTitle, afterFirstAccordioSubTitle)

        self.assertNotEqual(secondAccordioTitle, afterSecondAccordioTitle)
        self.assertNotEqual(secondAccordioSubTitle, afterSecondAccordioSubTitle)